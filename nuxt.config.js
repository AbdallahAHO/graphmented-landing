module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Graphmented - Create stunning charts using AR',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Graphmented - Create stunning charts using AR' },

      // Facebook Open Graph
      { property: 'og:title', content: 'Graphmented - Create stunning charts using AR' },
      { property: 'og:description', content: 'Transform your whole desk into a spreadsheets workstation. Drop sheets and charts on your desk as if they are real objects and make use of your whole desk space' },
      { property: 'og:type', content: 'website' },
      { property: 'og:site_name', content: 'Graphmented' },

      // Browser - chrome
      { name: 'theme-color', content: '#ce2453' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Poppins|Source+Sans+Pro' },
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  plugins: [
    '~/plugins/vue-js-modal'
  ],
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    '@nuxtjs/bootstrap-vue',
    // Or if you have custom bootstrap CSS...
    ['@nuxtjs/bootstrap-vue', { css: false }]
  ]
}
